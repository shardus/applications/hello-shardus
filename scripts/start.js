const archiverPath = require.resolve('@shardus/archiver')
const monitorPath = require.resolve('@shardus/monitor-server')
const shell = require('shelljs')

console.log('archiverPath, monitorPath:', archiverPath, monitorPath)

async function main () {
  try {
    shell.exec(`yarpm run pm2 start --no-autorestart ${archiverPath}`)
    shell.exec(`yarpm run pm2 start --no-autorestart ${monitorPath}` )
    console.log()
    console.log('\x1b[33m%s\x1b[0m', 'View network monitor at:') // Yellow
    console.log('  http://localhost:\x1b[32m%s\x1b[0m', '3000') // Green
    console.log()
  } catch (e) {
    console.log(e)
  }
}
main()
