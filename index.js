// import { shardusFactory, ShardusTypes } from '@shardus/core' // TODO bring back when SGS is exporting types
const { shardusFactory } = require('@shardus/core')
const crypto = require('@shardus/crypto-utils')

// It doesn't matter what 32 byte hex string you put in here, but you must have
// the same one for each node, otherwise every node will get a different hash
// when it tries to hash accounts. It's basically a salt.
// A note to the wise: Never change this value once your app is live, lest you
// want your new nodes to speak a different language from your old nodes.
crypto.init('abcd123abcd123abcdef3123456e5083934424abcfab9eee8765423111111111')

// This is the minimum config necessary to start shardus at the moment.
// See more at https://docs.shardus.com/docs/gettingstarted#shardus-configuration
const dapp = shardusFactory({
  server: {
    p2p: {
      minNodesToAllowTxs: 1
    },
  },
}) // There's a type mismatch here which forces us to cast this to any. In future versions this will be fixed.

//
// Helpers
//
const createAccount = (accountNumber) => {
  return {
    id: accountNumber,
    timestamp: Date.now(),
    state: 0 // This here represents our initial state
  }
}

const hashAccount = (account) => crypto.hashObj(account)
const hashTx      = (tx) => crypto.hashObj(tx)

// This will serve as our database for this example.
// It is local and in memory, but you can use any type of data storage
// you want using Shardus. Much of what you'll see in this file is a mapping
// of your state, however you choose to set it up, and the state within
// the shardus network.
let accounts = {}

//
//  Route registration
//

// This is where transactions will be injected into the network
// This will look the same for most apps.
dapp.registerExternalPost('inject', (req, res) => {
  try {
    const response = dapp.put(req.body)
    res.json(response)
  } catch (err) {
  }
})

// This is our one 'read' route. When this request comes in,
// we'll just pass back the entire contents of our "database".
dapp.registerExternalGet('state', (req, res) => {
  res.json(accounts)
})

//
// Dapp Instantiation
//
// This is where the magic happens. It's here that we create the glue
// that binds our state with shardus's internal representation of our state.
//

dapp.setup({

  // This is where you ensure transactions coming from the wild are shaped the
  // way you want them to be. If anything is off, you can return `{ success:
  // false, reason: '...' }`. Whatever this returns will be immediately sent
  // back to the client as their http response.
  // More: https://docs.shardus.com/docs/gettingstarted#validate
  validate(tx) {
    // In this example, we only need to make sure the tx has a numerical state
    // field.
    if (
      typeof tx.state !== 'number' // we're only dealing with numbers here
      || typeof tx.accountId !== 'string' // gotta supply the accountId or we're sunk!
      || tx.accountId.length !== 64 // 32 byte hex means 64 unicode characters
    ) {
      return {
        success: false,
        reason: 'Your transaction must have the shape { state: number, accountId: string }, with accountId being a 32 byte hex string',
      }
    } else {
      return {
        success: true,
        reason: '',
      }
    }
  },

  // This is a reducer function, it's where you take a transaction that's come
  // in from the outside world and mutate your state with it. This function
  // can either return an ApplyResponse, which is needed for the underlying
  // shardus network to function, or it can throw to signify something went
  // wrong. When modifying your state, be sure to modify `dataAccounts` rather
  // than your local state, otherwise shardus will have no way of knowing
  // what the updates were. Note that dataAccounts here represents a mutable
  // reference to an object, so you can modify it and shardus will know
  // without you having to pass it back as a return argument.
  // More: https://docs.shardus.com/docs/gettingstarted#apply
  // For more advanced uses, see Shardeum or Liberdus.
  // (https://gitlab.com/shardeum)
  // (https://gitlab.com/liberdus)
  apply(tx, dataAccounts) {
    dataAccounts[tx.accountId].data.state = tx.state
    const txId = hashTx(tx)
    return dapp.createApplyResponse(txId, tx.timestamp)
  },

  // This is how shardus "cracks open" the transaction to figure out what
  // account keys are involved so it can determine whether it needs to go
  // outside of the local shard to interact with that state.
  // More: https://docs.shardus.com/docs/examples/coin-app-template#crack
  crack(tx) {
    // In a more complex action involving multiple accounts,
    // the account ids in question here would be listed someowhere
    // on the transaction. Maybe in a coin app for a send coins
    // transaction, tx.to and tx.from would be populated.

    return {
      id: hashTx(tx),
      timestamp: tx.timestamp,
      keys: {
        sourceKeys: [tx.accountId], // [tx.from]
        targetKeys: [tx.accountId], // [tx.to]
        allKeys: [tx.accountId], // This is a concat/dedup of sourceKeys and targetKeys.
        timestamp: tx.timestamp // Note that in future versions shardus will dedup this key
      }
    }
  },

  // This is where we set our local data that's come from shardus.  This'll
  // happen for shardus network data repair, if another node has processed an
  // apply, and a number of other places. If shardus needs us to to set our
  // local state, it calls this.
  // More: https://docs.shardus.com/docs/gettingstarted#getaccountdata
  setAccountData (accountsToSet) {
    accountsToSet.forEach(account => accounts[account.id] = account)
  },

  // For now, this is required but can have the same code as setAccountData.
  // In the future, this will be removed or modified.
  // More: https://docs.shardus.com/docs/gettingstarted#resetaccountdata
  resetAccountData(accountsToSet) {
    this.setAccountData(accountsToSet)
  },

  // This is pretty self explanatory - shardus will give us a list of account
  // addresses for us to delete. It's a continuation of the pattern of us
  // supplying a mapping of our data for to the shardus network.
  // More: https://docs.shardus.com/docs/gettingstarted#deleteaccountdata
  deleteAccountData(addressList) {
    for (const address of addressList) {
      delete accounts[address]
    }
  },

  // Into this function we put the code to clear all state from the database,
  // or in this case, our in memory objet. shardus needs this function for a
  // specific use case during a node's sync phase, where it's coming into
  // agreement about the state of the world with the other nodes.
  // More: https://docs.shardus.com/docs/gettingstarted#deletelocalaccountdata
  deleteLocalAccountData() {
    accounts = {}
  },

  // This function gets called during shardus's "commit" phase, which is when
  // it takes an ApplyResponse and instructs the rest of the network to update
  // its state with it.  Note that while similar to `setAccountData`, this
  // function is used for a different phase in shardus's under the hood
  // protocols.
  // More: https://docs.shardus.com/docs/gettingstarted#updateaccountfull
  updateAccountFull(wrappedState, localCache, applyResponse) {
    const { accountId, accountCreated } = wrappedState
    const updatedAccount = wrappedState.data 

    const hashBefore = accounts[accountId] ? hashAccount(accounts[accountId]) : '' // Can't pass in undefined
    const hashAfter  = hashAccount(updatedAccount)

    // Update our local state
    accounts[accountId] = updatedAccount

    // See more: https://docs.shardus.com/docs/gettingstarted#updateaccountfull
    dapp.applyResponseAddState(
      applyResponse,
      updatedAccount,
      localCache,
      accountId,
      applyResponse.txId,
      applyResponse.txTimestamp,
      hashBefore,
      hashAfter,
      accountCreated
    )
  },

  // This is similar to updateAccountFull, except it's a sort of PATCH-like update, where not
  // all fields on a potentially large Account object are being given. For this
  // and other simple apps, updateAccountPartial won't need its own logic.
  // More: https://docs.shardus.com/docs/gettingstarted#updateaccountpartial
  updateAccountPartial(wrappedAccount, localCache, applyResponse) {
    this.updateAccountFull(wrappedAccount, localCache, applyResponse)
  },

  // Similar to `updateAccountPartial`, this function allows the dapp developer
  // to take just a chunk of an account that's relevant to a transaction and
  // return that to the network.
  // More: https://docs.shardus.com/docs/gettingstarted#getrelevantdata
  getRelevantData(accountId, tx) {
    let account = accounts[accountId]
    let accountCreated = false

    // Use createAccount to create the account if it doesn't exist, that logic
    // has to exist here, Even if as a dapp developer you never delete your
    // account, this logic must exist because `shardus` may delete the account
    // in its sync phase.
    if (!account) {
      account = createAccount(accountId)
      accountCreated = true
    }

    const accountHash = hashAccount(account)

    return dapp.createWrappedResponse(accountId, accountCreated, accountHash, account.timestamp, account)
  },

  // This is a request from the shardus network to retrieve all records within
  // a certain range, as measured by linear distance between 32 bit hex account
  // ids. Being able to get a range of data from a node's stores like this enables
  // the network to store data in different shards.
  // A note on `maxRecords`: Some larger applications will have many records,
  // of potentially large size. Only having to return a finite number
  // helps speed up your big app dramatically.
  // More: https://docs.shardus.com/docs/gettingstarted#getaccountdata
  getAccountData (accountStart, accountEnd, maxRecords) {
    const wrappedAccounts = []

    // We'll parse these for later numerical comparisons
    const start = parseInt(accountStart, 16)
    const end = parseInt(accountEnd, 16)

    for (const account of Object.values(accounts)) {
      const accountId = parseInt(account.id, 16)

      // Skip if not in account id range
      // When `shardus` synchronizes data, it'll ask for some number of
      // accounts at a time. This is how we give `shardus` that range of accounts.
      if (accountId < start || accountId > end) continue

      const wrappedAccount = dapp.createWrappedResponse(
        account.id,
        false,
        hashAccount(account),
        account.timestamp,
        account
      )

      wrappedAccounts.push(wrappedAccount)

      if (wrappedAccounts.length >= maxRecords) return wrappedAccounts
    }


    return wrappedAccounts
  },

  // This is for shardus to sync account data. Using the timestamp information here
  // helps shardus shard in a busy network. The goal of this function is to get all the
  // data in our local storage that's within range of an accountId and within a certain
  // chronological timeframe.
  // More: https://docs.shardus.com/docs/gettingstarted#getaccountdatabyrange
  getAccountDataByRange(accountStart, accountEnd, tsStart, tsEnd, maxRecords) {
    const wrappedAccounts = []

    const start   = parseInt(accountStart, 16)
    const end     = parseInt(accountEnd, 16)

    for (const account of Object.values(accounts)) {

      // Skip if not in account id range
      const id = parseInt(account.id, 16)
      if (id < start || id > end) continue

      // Skip if not in timestamp range
      const timestamp = account.timestamp
      if (timestamp < tsStart || timestamp > tsEnd) continue

      const wrappedAccount = dapp.createWrappedResponse(
        account.id,
        false,
        hashAccount(account),
        account.timestamp,
        account
      )

      wrappedAccounts.push(wrappedAccount)

      // Return results early if maxRecords reached
      if (wrappedAccounts.length >= maxRecords) return wrappedAccounts
    }

    return wrappedAccounts
  },

  // Here shardus is asking us to give it a list of accounts that
  // we've stored locally on our node. One node will be in a shard,
  // so will likely only have some of this list. We give shardus what
  // we have, and other nodes in other shards will give what they have,
  // until shardus gets the whole shebang.
  // More: https://docs.shardus.com/docs/gettingstarted#getaccountdatabylist
  getAccountDataByList(addressList) {
    let wrappedAccounts = []

    for (const address of addressList) {
      const account = accounts[address]

      if (!account) continue

      const wrappedAccount = dapp.createWrappedResponse(
        account.id,
        false,
        hashAccount(account),
        account.timestamp,
        account
      )

      wrappedAccounts.push(wrappedAccount)
    }

    return wrappedAccounts
  },

  // This allows shardus to maintain the veracity of your state when passed
  // around the network.
  // More: https://docs.shardus.com/docs/gettingstarted#calculateaccounthash
  calculateAccountHash(account) {
    return hashAccount(account)
  },

  // This allows you to serialize your account into a string for later viewing
  // within the logs.
  // More: https://docs.shardus.com/docs/api/interface/setup/getAccountDebugValue
  getAccountDebugValue(wrappedAccount) {
    return `${JSON.stringify(wrappedAccount.data)}`
  },

  // Here we can add any functionality we want for when our node is shutting down.
  // This can be an empty function if you like, or maybe it can contain a notification system
  // to send you an SMS or email. Go wild! Note that you can return a Promise here and
  // the app will wait for your promise to resolve before killing the process.
  // More: https://docs.shardus.com/docs/gettingstarted#close
  close() { console.log('Shutting down...') },
})

dapp.registerExceptionHandler()
dapp.start()
